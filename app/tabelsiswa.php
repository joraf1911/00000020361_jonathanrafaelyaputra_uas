<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tabelsiswa extends Model
{
    protected $table = "tabelsiswas"; 
    protected $primaryKey = "id";
    public $timestamps = false;
}

