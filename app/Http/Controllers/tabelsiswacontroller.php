<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\tabelsiswa;

class tabelsiswacontroller extends Controller
{
	public function index($message = NULL){
		$tabelSiswas = tabelsiswa::all();
		return view("halamanutama",compact('tabelSiswas','message')); 
	}

	public function posthandling(Request $request){
		$command = $request["submission"];

		if($command == "Update"){
			$idfind = $request["idUpdate"];
			$siswaUpdate = tabelsiswa::find($idfind);

			return view(
				"formsiswa",compact('siswaUpdate','command')
			);

		} else if ($command == "Delete"){
			$idfind = $request["idUpdate"];
			$siswaDelete = tabelsiswa::find($idfind);
			return view(
				"suredelete",compact('siswaDelete','command')
			);
		}
	}

	public function update(Request $request){

		if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
			$errors = new MessageBag;
			$errors->add("submission_error", "Belum ada verifikasi CAPTCHA!");
			return view('errorhandling')->withErrors($errors);
		} else {
			$verifyURL = "https://www.google.com/recaptcha/api/siteverify";
			$verifyURL .= "?secret=6LfWPa0UAAAAAIJj8UGZrzjv-uLScmh5j7RsCyuh";
			$verifyURL .= "&response=" . $request["g-recaptcha-response"];
			$verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
			$verifyResponse = file_get_contents($verifyURL);
			$responseParse = (array) json_decode($verifyResponse);
			if ($responseParse["success"] == false) {
				$errors = new MessageBag;
				$errors->add("submission_error", "Validasi CAPTCHA Error!!!");
				return view('errorhandling')->withErrors($errors);
			}
		}

		$message = NULL;

		$validatedData = $request->validate([
			'nama' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
			'tempatlahir' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
			'tanggallahir' => 'required|date|after_or_equal:2006-01-01|before_or_equal:2013-12-31',
			'jeniskelamin' => 'required',
			'alamat' => 'required|max:250',
		]);

		$idfind = $request["idUpdate"];

		$siswaUpdate = tabelsiswa::find($idfind);

		$siswaUpdate -> nama = $request["nama"]; 
		$siswaUpdate -> tempatlahir = $request["tempatlahir"];
		$siswaUpdate -> tanggallahir = $request["tanggallahir"];
		$siswaUpdate -> jeniskelamin = $request["jeniskelamin"];
		$siswaUpdate -> alamat = $request["alamat"];
		$siswaUpdate -> save();

		$message = "Data siswa dengan NIS berupa " . $siswaUpdate->nis . " telah diperbaharui";

		return $this->index($message);

	}

	public function insert(Request $request){

		if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
			$errors = new MessageBag;
			$errors->add("submission_error", "Belum ada verifikasi CAPTCHA!");
			return view('errorhandling')->withErrors($errors);
		} else {
			$verifyURL = "https://www.google.com/recaptcha/api/siteverify";
			$verifyURL .= "?secret=6LfWPa0UAAAAAIJj8UGZrzjv-uLScmh5j7RsCyuh";
			$verifyURL .= "&response=" . $request["g-recaptcha-response"];
			$verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
			$verifyResponse = file_get_contents($verifyURL);
			$responseParse = (array) json_decode($verifyResponse);
			if ($responseParse["success"] == false) {
				$errors = new MessageBag;
				$errors->add("submission_error", "Validasi CAPTCHA Error!!!");
				return view('errorhandling')->withErrors($errors);
			}
		}

		$message = NULL;

		$validatedData = $request->validate([
			'nama' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
			'nis' => 'required|numeric|unique:tabelsiswas,nis|digits:8',
			'tempatlahir' => 'required|regex:/^[\pL\s\-]+$/u|max:50',
			'tanggallahir' => 'required|date|after_or_equal:2006-01-01|before_or_equal:2013-12-31',
			'jeniskelamin' => 'required',
			'alamat' => 'required|max:250',
		]);

		$siswaInsert = new tabelsiswa; 
		$siswaInsert -> nama = $request["nama"]; 
		$siswaInsert -> nis = $request["nis"];
		$siswaInsert -> tempatlahir = $request["tempatlahir"];
		$siswaInsert -> tanggallahir = $request["tanggallahir"];
		$siswaInsert -> jeniskelamin = $request["jeniskelamin"];
		$siswaInsert -> alamat = $request["alamat"];
		$siswaInsert -> save();

		$message = "Data siswa dengan NIS berupa " . $siswaInsert->nis . " telah ditambahkan";

		return $this->index($message);
	}

	public function delete(Request $request){

		$message = NULL;

		$command = $request["submission"]; 

		$idfind = $request["idUpdate"];

		$siswaDelete = tabelsiswa::find($idfind);


		if($command == "Yes"){

			if (!isset($request["g-recaptcha-response"]) || empty($request["g-recaptcha-response"])) {
				$errors = new MessageBag;
				$errors->add("submission_error", "Belum ada verifikasi CAPTCHA!");
				return view('errorhandling')->withErrors($errors);
			} else {
				$verifyURL = "https://www.google.com/recaptcha/api/siteverify";
				$verifyURL .= "?secret=6LfWPa0UAAAAAIJj8UGZrzjv-uLScmh5j7RsCyuh";
				$verifyURL .= "&response=" . $request["g-recaptcha-response"];
				$verifyURL .= "&remoteip=" . $_SERVER["REMOTE_ADDR"];
				$verifyResponse = file_get_contents($verifyURL);
				$responseParse = (array) json_decode($verifyResponse);
				if ($responseParse["success"] == false) {
					$errors = new MessageBag;
					$errors->add("submission_error", "Validasi CAPTCHA Error!!!");
					return view('errorhandling')->withErrors($errors);
				}
			}


			$message = "Data siswa dengan NIS berupa " . $siswaDelete->nis . " telah dihapus";

			$siswaDelete -> delete();

			return $this->index($message);
		} else if ($command == "No"){
			$message = "Penghapusan data siswa dengan NIS berupa " . $siswaDelete->nis . " telah dibatalkan";
			return $this->index($message);
		}

	}


}
