<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/',"tabelsiswacontroller@index");

Route::get('/halamanutama',"tabelsiswacontroller@index");

Route::post('/backtohome',"tabelsiswacontroller@index");

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/submitForm',"tabelsiswacontroller@posthandling");

Route::post('/submitPerubahanData',"tabelsiswacontroller@update");

Route::post('/submitPerubahanData2',"tabelsiswacontroller@insert");

Route::post('/submitPerubahanData3',"tabelsiswacontroller@delete");

Route::get('/submitForm', function(){
    return view('errorhandling');
});