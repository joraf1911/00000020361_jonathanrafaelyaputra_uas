<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabelsiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabelsiswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama',191);
            $table->string('nis',191)->unique();
            $table->string('tempatlahir');
            $table->date('tanggallahir');
            $table->enum('jeniskelamin', ['Laki-laki', 'Perempuan']);
            $table->text('alamat');
        });

        DB::statement("ALTER TABLE tabelsiswas ADD image MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
