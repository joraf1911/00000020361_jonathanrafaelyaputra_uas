mysql -u root

CREATE DATABASE siswa;

USE siswa;

SHOW DATABASES;

CREATE TABLE tabelsiswas (

	id INT PRIMARY KEY AUTO_INCREMENT,
	nama VARCHAR(50),
	nis VARCHAR(8) UNIQUE,
	tempatlahir VARCHAR (50),
	tanggallahir DATE,
	jeniskelamin ENUM('Laki-laki','Perempuan'),
	alamat TEXT
	
) engine=InnoDB;

INSERT INTO tabelsiswas(nama,nis,tempatlahir,tanggallahir,jeniskelamin,alamat)
VALUES ('Jonathan Rafael','00192847', 'Jakarta', '2005-3-20', 'Laki-laki','Jl. Gunung Mas 5 Blok 3');

ALTER TABLE tabelsiswas ADD image MEDIUMBLOB AFTER alamat;