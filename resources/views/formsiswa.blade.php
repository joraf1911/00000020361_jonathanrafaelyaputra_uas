@extends('layouts.app')

@section('content')
<div class="container">

@if(isset($siswaUpdate -> id))

    <h1>Update Form</h1>

    <form id="formSiswa" method="POST" action="submitPerubahanData" name="formSiswa">
        @csrf
        <input type=hidden name="idUpdate" value="{{$siswaUpdate -> id}}">
        <div class="form-group">
            <label>
                Nama
                <input name="nama" type="text" placeholder="Write Name Here" value='{{$siswaUpdate -> nama}}'  class="form-control" style="width: 500px;"> 
            </label>        
        </div>
        <div class="form-group">
            <label>
                NIS
                <p>{{$siswaUpdate -> nis}}</p>
            </label>        
        </div>
        <div class="form-group">
            <label>
                Tempat Lahir
                <input name="tempatlahir" type="text" placeholder="Write BirthPlace Here" value='{{$siswaUpdate -> tempatlahir}}'  class="form-control" style="width: 500px;">   
            </label>        
        </div>
        <div class="form-group">
            <label>
                Tanggal Lahir
                <input name="tanggallahir" type="date" placeholder="Write Date Here" value='{{$siswaUpdate -> tanggallahir}}'  class="form-control" style="width: 500px;">   
            </label>        
        </div>
        <div class="form-group">
            <label>
                Jenis Kelamin
                <select name="jeniskelamin" class="form-control" id="jeniskelamin" placeholder="Masukkan Jenis Kelamin" value="{{$siswaUpdate -> jeniskelamin}}">
                    <option <?php if ($siswaUpdate["jeniskelamin"]=="Laki-laki") echo "selected";?>>Laki-Laki</option>
                    <option <?php if ($siswaUpdate["jeniskelamin"]=="Perempuan") echo "selected";?>>Perempuan</option>
                </select>
            </label> 

        </div>
        <div class="form-group">
            <label>
                Alamat
                <textarea name="alamat"  type="text" placeholder="Write Address Here" class="form-control" id="alamat" style="width: 500px;">{{$siswaUpdate -> alamat}}</textarea>  
            </label>        
        </div>
        <div class="g-recaptcha" data-sitekey="6LfWPa0UAAAAACNJpdm6y3wTJVE8iwWs9fuuQcpV" style="margin-bottom: 10px;"> </div>
        <input type="submit" name="submission" value="Update" class="btn btn-warning" style="margin-bottom: 10px;">
    </form>
@else 
    <h1>Insert Form</h1>

    <form id="formSiswa" method="POST" action="submitPerubahanData2" name="formSiswa">
        @csrf
        <div class="form-group">
            <label>
                Nama
                <input name="nama" type="text" placeholder="Write Name Here" class="form-control" style="width: 500px;"> 
            </label>        
        </div>
        <div class="form-group">
            <label>
                NIS
                <input name="nis" type="text" placeholder="Write NIS Here" class="form-control" style="width: 500px;"> 
            </label>        
        </div>
        <div class="form-group">
            <label>
                Tempat Lahir
                <input name="tempatlahir" type="text" placeholder="Write BirthPlace Here"  class="form-control" style="width: 500px;">   
            </label>        
        </div>
        <div class="form-group">
            <label>
                Tanggal Lahir
                <input name="tanggallahir" type="date" placeholder="Write Date Here" class="form-control" style="width: 500px;">   
            </label>        
        </div>
        <div class="form-group">
            <label>
                Jenis Kelamin
                <select name="jeniskelamin" class="form-control" id="jeniskelamin" placeholder="Write Gender Here" >
                    <option value="Laki-laki">Laki-Laki</option>
                    <option value="Perempuan">Perempuan</option>
                </select>
            </label>        
        </div>
        <div class="form-group">
            <label>
                Alamat
                <textarea name="alamat"  type="text" placeholder="Write Address Here"  class="form-control" id="alamat" style="width: 500px;"></textarea>   
            </label>        
        </div>
         <div class="g-recaptcha" data-sitekey="6LfWPa0UAAAAACNJpdm6y3wTJVE8iwWs9fuuQcpV" style="margin-bottom: 10px;"> </div>
        <input type="submit" name="submission" value="Insert" class="btn btn-warning" style="margin-bottom: 10px;">
    </form>

@endif
</div>
@endsection
