@extends("layouts.app")

@section("content")



<div class="container">


@if(isset($siswaDelete -> id))
	<h5>Konfirmasi Penghapusan</h5>
            <table id="tableDelete" class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nama Siswa</th>
                    <th>NIS</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    </tr>
                </thead>
                <tbody>
                  
                    <tr>
                        <td>{{$siswaDelete->nama}}</td>
                    <td>{{$siswaDelete->nis}}</td>
                    <td>{{$siswaDelete->tempatlahir}}</td>
                    <td>{{$siswaDelete->tanggallahir}}</td>
                    <td>{{$siswaDelete->jeniskelamin}}</td>
                    <td>{{$siswaDelete->alamat}}</td>
                    </tr>
                    
                </tbody>
            </table>

<form id="suretoDelete" method="POST" action="submitPerubahanData3" name="suretoDelete">
	@csrf
	<div class="g-recaptcha" data-sitekey="6LfWPa0UAAAAACNJpdm6y3wTJVE8iwWs9fuuQcpV" style="margin-bottom: 10px;"> </div>
	<input type=hidden name="idUpdate" value="{{$siswaDelete -> id}}">
	<p>Anda yakin ingin menghapus data ini? </p>
	<input type="submit" name="submission" value="Yes" class="btn btn-success" style="margin-bottom: 10px;">
	<input type="submit" name="submission" value="No" class="btn btn-danger" style="margin-bottom: 10px;">
</form>
@else 

<h3>Pilih data yang ingin dihapus terlebih dahulu!</h3>


@endif

</div>

@endsection