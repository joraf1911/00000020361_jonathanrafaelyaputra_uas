@extends('layouts.app')

@section('content')

<div class="container">
    @if(isset($message))
        <h5>{{$message}}</h5>
    @endif
    <form method="POST" id="formUtama" action="submitForm">
        <table id="tableData" class="table table-striped table-bordered">
            <thead>
                <tr>

                    @auth

                    <th> </th>

                    @endauth
                    <th>Nama Siswa</th>
                    <th>NIS</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                </tr>
                
            </thead>

            <tbody>

                @foreach ($tabelSiswas as $tabelsiswa)
                <tr>
                    @auth
                    <td> 
                        <input class='select-checkbox' type='radio' name='idUpdate' value='{{$tabelsiswa->id}}'>
                    </td>
                    @endauth
                    <td>{{$tabelsiswa->nama}}</td>
                    <td>{{$tabelsiswa->nis}}</td>
                    <td>{{$tabelsiswa->tempatlahir}}</td>
                    <td>{{$tabelsiswa->tanggallahir}}</td>
                    <td>{{$tabelsiswa->jeniskelamin}}</td>
                    <td>{{$tabelsiswa->alamat}}</td>
                </tr>

                @endforeach
            </tbody>
        </table>
        @auth
        @csrf
        <input type="submit" name="submission" value="Update" class="btn btn-success" style="margin-bottom: 10px;">
        <input type="submit" name="submission" value="Delete" class="btn btn-danger" style="margin-bottom: 10px;">
        @endauth
    </form>
</div>

<script>

    $(document).ready(function(){
        $('#tableData').DataTable({
            columnDefs: [{
                orderable: false
            }]
        })
    });

</script>
@endsection
