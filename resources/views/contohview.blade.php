@extends("layouts.appcontoh")

@section("content")

<div class="container">

	@if(isset($message))
        <h3>{{$message}}</h3>
    @endif

	<form method="POST" id="selectProduct" action="postProduct">



	<table id="tableData" class="table table-striped table-bordered">
			<thead>
				<tr>

					@auth

					<th> </th>

					@endauth
					<th>ID</th>
					<th>Product Name</th>
					<th>Quantity Per Unit</th>
					<th>Price</th>
					<th>Stock</th>
				</tr>
				
			</thead>

			<tbody>

			@foreach ($saleItems as $saleItem)
			<tr>
				@auth
				<td> 
					<input class='select-checkbox' type='radio' name='idUpdate' value='{{$saleItem->ID_Item}}'>
				</td>
				@endauth
				<td>{{$saleItem->ID_Item}}</td>
				<td>{{$saleItem->productName}}</td>
				<td>{{$saleItem->quantityPerUnit}}</td>
				<td>{{$saleItem->price}}</td>
				<td>{{$saleItem->stock}}</td>
			</tr>

			@endforeach
		</tbody>
	</table>

	@auth
	@csrf
	<input type="submit" name="submission" value="Update" class="btn btn-warning" style="margin-bottom: 10px;">
	<input type="submit" name="submission" value="Delete" class="btn btn-danger" style="margin-bottom: 10px;">
	@endauth

</form>
</div>

<script>
	
	$(document).ready(function(){
		$('#tableData').DataTable({
			columnDefs: [{
				orderable: false
			}]
		})
	})

</script>

@endsection