<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link href="{{ asset('css/logincss.css') }}" rel="stylesheet">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

</head>


<body>
    <div class="simple-login-container">
        <h2>Registration Form</h2>
        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="row">


                <div class="col-md-12 form-group">
                    <input id="name" type="text" placeholder="Register Name Here" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 form-group">
                 <input id="email" type="email" placeholder="Register Email Here" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                 @error('email')
                 <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="row">

            <div class="col-md-12 form-group">
                <input id="password" type="password" placeholder="Register Password Here" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="row">

            <div class="col-md-12 form-group">

                <input id="password-confirm" type="password" placeholder="Reconfirm Password Here" class="form-control" name="password_confirmation" required autocomplete="new-password">

            </div>
        </div>


        <div class="row">
            <div class="col-md-12 form-group">
                <button type="submit" class="btn btn-danger">
                    {{ __('Register') }}
                </button>
                <button type="submit" class="btn btn-danger" onclick="location.href='{{ url('/') }}'">

                        Back to Home
                        
                    </button>

            </div>
        </div>
    </form>
</div>

</body>

</html>
