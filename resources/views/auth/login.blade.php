<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <link href="{{ asset('css/logincss.css') }}" rel="stylesheet">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Laravel</title>

</head>
<body>
    <div class="simple-login-container">
        <h2>Login Form</h2>
        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="row">


                <div class="col-md-12 form-group">
                    <input id="email" type="email" placeholder="Input Email Here" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 form-group">
                    <input id="password" type="password" placeholder="Input Password Here" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>


            <div class="row">
                <div class="col-md-12 form-group">
                    <button type="submit" class="btn btn-danger">
                        {{ __('Login') }}
                    </button>

                    <button type="submit" class="btn btn-danger" onclick="location.href='{{ url('/') }}'">

                        Back to Home
                        
                    </button>

                </div>
            </div>
        </form>
    </div>
    
</body>
</html>
